# Changelog

#### 2.1.4

- Chore: support base `3.0`.

#### 2.1.3

- Chore: support base 2.1 (compiler v3).

#### 2.1.2

- Chore: support base 2.0.

#### 2.1.1

- Fix: fix selectChC implementation for Windows.

### 2.1.0

- Chore: update version of base-rts to 2.0.

#### 2.0.1

- Fix: remove Clean.h files, add clean-rts depedency (which provides Clean.h).

## 2.0.0

- BROKEN DUE TO MISSING CLEAN-RTS DEPENDENCY!.
- Initial clean-lang.org version.
